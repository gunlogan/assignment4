﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Attack : MonoBehaviour
{
    public GameObject _projectile;
    public float _projectileSpeed;

    private void Update() {
        if (Input.GetKeyDown(KeyCode.Space)) {
            GameObject fireBall = Instantiate(_projectile, transform) as GameObject;
            Rigidbody rb = fireBall.GetComponent<Rigidbody>();
            rb.velocity = transform.forward * _projectileSpeed;
        }
    }
}

//Axis1D.SecondaryIndexTrigger