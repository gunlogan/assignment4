﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Lerper : MonoBehaviour
{
    public Transform[] _targets;
    private int _nextTargetIndex;
    private float _lerpTime = 3f;
    private bool _isMoving;

    public void LerpByIndex(int index)
    {
        if (!_isMoving) {
            StartCoroutine(MoveCoroutine(_targets[_nextTargetIndex]));
            _nextTargetIndex++;
            if (_nextTargetIndex == _targets.Length) {
              _nextTargetIndex = 0;
            }
        }
    }

    private IEnumerator MoveCoroutine(Transform target) {
        Debug.Log("Starting coroutine to move");
        _isMoving = true;
        Vector3 startPosition = transform.position;
        Vector3 startScale = transform.localScale;
        Quaternion startRotation = transform.rotation;
        float counter = 0;
        while (counter < 1f) {
            counter += Time.deltaTime / _lerpTime;
            transform.position = Vector3.Lerp(startPosition, target.position, counter);
            transform.localScale = Vector3.Lerp(startScale, target.localScale, counter);
            transform.rotation = Quaternion.Lerp(startRotation, target.rotation, counter);
            yield return 0;
        }
        _isMoving = false;
    }
}
