﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RaycastLooker : MonoBehaviour
{
    public TextMesh _objectName;

    void FixedUpdate() {
        _objectName.text = "";

        RaycastHit raycastHit;
        if (Physics.Raycast(transform.position, transform.forward, out raycastHit)) {
            if (raycastHit.rigidbody != null) {
                _objectName.text = raycastHit.transform.name;
            }

        }
    }
}
