﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TouchController : MonoBehaviour{

    public OVRInput.Controller _controller;

    // Update is called once per frame
    void Update() {
        transform.localPosition = OVRInput.GetLocalControllerPosition(_controller);
        transform.localRotation = OVRInput.GetLocalControllerRotation(_controller);
    }
}
