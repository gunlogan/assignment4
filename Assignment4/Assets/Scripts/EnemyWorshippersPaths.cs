﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyWorshippersPaths : MonoBehaviour
{
    public static Transform[] _paths;

    private void Awake() {
        _paths = new Transform[transform.childCount];
        for(int i = 0; i <_paths.Length; i++) {
            _paths[i] = transform.GetChild(i);
        }
    }
}
