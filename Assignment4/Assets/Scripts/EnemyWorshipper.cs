﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyWorshipper : MonoBehaviour {

    public float _speed = 2f;

    private Transform _target;
    private int _pathIndex = 0;

    private void Start() {
        _target = EnemyWorshippersPaths._paths[0];
    }

    private void Update() {
        Vector3 direction = _target.position - transform.position;
        transform.Translate(direction.normalized * _speed * Time.deltaTime, Space.World);

        if (Vector3.Distance(transform.position, _target.position) <= 0.2f) {
            GetNextPath();
        }
    }

    void GetNextPath() {
        if (_pathIndex >= EnemyWorshippersPaths._paths.Length -1) {
            Destroy(gameObject);
            return;
        }

        _pathIndex++;
        _target = EnemyWorshippersPaths._paths[_pathIndex];
    }
}
