﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RightHandGrab : MonoBehaviour
{
    public OVRInput.Controller _controller;
    private GameObject _grabbedObject;
    private bool _grabbing;
    public float _grabRadius;
    public LayerMask _grabMask;

    private Quaternion _lastRotation, _currentRotation;
    

    void GrabObject() {
        _grabbing = true;

        RaycastHit[] hits;

        hits = Physics.SphereCastAll(transform.position, _grabRadius, transform.forward, 0f, _grabMask);

        if (hits.Length > 0) {

            int closestHit = 0;

            for (int i = 0; i < hits.Length; i++) {
                if (hits[i].distance < hits[closestHit].distance) closestHit = i; 
            }

            _grabbedObject = hits[closestHit].transform.gameObject;
            _grabbedObject.GetComponent<Rigidbody>().isKinematic = true;
            _grabbedObject.transform.position = transform.position;
            _grabbedObject.transform.parent = transform;
        }
    }


    void DropObject() {
        _grabbing = false;

        if (_grabbedObject != null) {
            _grabbedObject.transform.parent = null;
            _grabbedObject.GetComponent<Rigidbody>().isKinematic = false;
            _grabbedObject.GetComponent<Rigidbody>().velocity = OVRInput.GetLocalControllerVelocity(_controller);
            _grabbedObject.GetComponent<Rigidbody>().angularVelocity = GetAngularVelocity();


            _grabbedObject = null;
        }
    }


    Vector3 GetAngularVelocity() {
        Quaternion deltaRotation = _currentRotation * Quaternion.Inverse(_lastRotation);
        return new Vector3(Mathf.DeltaAngle(0, deltaRotation.eulerAngles.x), Mathf.DeltaAngle(0, deltaRotation.eulerAngles.y), Mathf.DeltaAngle(0, deltaRotation.eulerAngles.z));
    }


    // Update is called once per frame
    void Update()
    {
        if (_grabbedObject != null) {
            _lastRotation = _currentRotation;
            _currentRotation = _grabbedObject.transform.rotation;
        }   

        if (!_grabbing & OVRInput.Get(OVRInput.Axis1D.PrimaryHandTrigger, _controller) == 1) {
            GrabObject();
        }

        if (_grabbing & OVRInput.Get(OVRInput.Axis1D.PrimaryHandTrigger, _controller) < 1) {
            DropObject();
        }
    }
}

