﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Waves : MonoBehaviour {

    public float _power = 3;
    public float _scale = 1;
    public float _timeScale = 1;

    private float _xOffset;
    private float _yOffset;
    private MeshFilter _mf;

    // Start is called before the first frame update
    void Start() {
        _mf = GetComponent<MeshFilter>();
        MakeWaves();
    }

    // Update is called once per frame
    void Update() {
        MakeWaves();
        _xOffset += Time.deltaTime * _timeScale;
        if (_yOffset <= 0.3) _yOffset += Time.deltaTime * _timeScale;
        if (_yOffset >= _power) _yOffset -= Time.deltaTime * _timeScale;

    }

    void MakeWaves() {
        Vector3[] verticies = _mf.mesh.vertices;

        for(int i = 0; i < verticies.Length; i++) {
            verticies[i].y = CalculateHeight(verticies[i].x, verticies[i].z) * _power;
        }

        _mf.mesh.vertices = verticies;
    }

    float CalculateHeight(float x, float y) {
        float xCord = x * _scale + _xOffset;
        float yCord = y * _scale + _yOffset;

        return Mathf.PerlinNoise(xCord, yCord);
    }
}
