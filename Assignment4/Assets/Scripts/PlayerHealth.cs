﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerHealth : MonoBehaviour
{
    public static int _lives;
    public int _startLives = 100;


    // Start is called before the first frame update
    void Start() {
        _lives = _startLives;
    }
}
