﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class CloudData {
    public Vector3 _position;
    public Vector3 _scale;
    public Quaternion _rotation;

    public bool isActive { get; private set; }

    public int x;
    public int y;
    public float distanceFromCamera;

    //returns matrix4x4 for the clouds
    public Matrix4x4 matrix {
        get {
            return Matrix4x4.TRS(_position, _rotation, _scale);
        }
    }

    //used to instanciate the clouds
    public CloudData(Vector3 pos, Vector3 scale, Quaternion rot, int x, int y, float distanceFromCamera) {
        this._position = pos;
        this._scale = scale;
        this._rotation = rot;
        SetActive(true);
        this.x = x;
        this.y = y;
        this.distanceFromCamera = distanceFromCamera;
    }

    public void SetActive(bool desState) {
        isActive = desState;
    }
}


public class CreateClouds : MonoBehaviour
{
    //Meshes
    public Mesh cloudMesh;
    public Material cloudMaterial;

    //Cloud Data
    public float cloudSize = 5;
    public float maximumScale = 1;

    //Noise Generation
    public float timeScale = 1;
    public float textureScale = 1;

    //Cloud Scaling Info
    public float minimumNoiseSize = 0.5f;
    public float sizeScale = 0.25f;

    //Culling Data
    public Camera cam;
    public int maximumDistance;

    //number Of Cloud Batches
    public int batchesToCreate;

    private Vector3 previousCameraPosition;
    private float offsetX = 1;
    private float offsetY = 1;
    //List within the List within the List
    private List<List<CloudData>> batches = new List<List<CloudData>>();
    private List<List<CloudData>> batchesToUpdate = new List<List<CloudData>>();

    private void Start() {
        for(int batchesX = 0; batchesX < batchesToCreate; batchesX++) {
            for(int batchesY = 0; batchesY < batchesToCreate; batchesY++) {
                BuildCloudBatch(batchesX, batchesY);
            }
        }
    }

    //Start Looping through X and Y values to generate a batch that is 32x32 clouds

    private void BuildCloudBatch(int xLoop, int yLoop) {
        bool markBatch = false;
        List<CloudData> currentBatch = new List<CloudData>();

        for (int x = 0; x < 31; x++) {
            for (int y = 0; y < 31; y++) {
                //Add a cloud for each loop
                AddCloud(currentBatch, x + xLoop * 31, y + yLoop * 31);
            }
        }

        //Check if the batch should be marked
        markBatch = CheckForActiveBatch(currentBatch);

        //Add the newest batch to the batches list
        batches.Add(currentBatch);

        //If the batch is marked add it to the batchesToUpdate list
        if (markBatch) batchesToUpdate.Add(currentBatch);
    }

    //this method checks to see if the current batch has a cloud that is within the camera range
    //return true if a cloud is within range
    //return false if no clouds are within range

    private bool CheckForActiveBatch(List<CloudData> batch) {
        foreach (var cloud in batch) {
            cloud.distanceFromCamera = Vector3.Distance(cloud._position, cam.transform.position);
            if (cloud.distanceFromCamera < maximumDistance) return true;
        }
        return false;
    }

    //This method created our clouds as a CloudData object
    private void AddCloud(List<CloudData> currentBatch, int x, int y) {
        
        //setting the new clouds position
        Vector3 position = new Vector3(transform.position.x + x * cloudSize, transform.position.y, transform.position.z + y * cloudSize);

        //Setting the clouds distance to the camera that can be used later
        float distanceToCamera = Vector3.Distance(new Vector3(x, transform.position.y, y), cam.transform.position);

        //Adding the new CloudData to the current batch
        currentBatch.Add(new CloudData(position, Vector3.zero, Quaternion.identity, x, y, distanceToCamera));
    }

    //Noise generator that updates and goes through the cloud objects 
    private void Update() {
        MakeNoise();
        offsetX += Time.deltaTime * timeScale;
        offsetY += Time.deltaTime * timeScale;
    }

    //This method updates the noise/clouds
    //Checking if the camera has moved
    //If it has not the batches are updated
    //If it has the prevoius camera position needs to be reset and update the batch list before updating batches

    void MakeNoise() {
        if (cam.transform.position == previousCameraPosition) {
            UpdateBatches();
        }
        else {
            previousCameraPosition = cam.transform.position;
            UpdateBatchList();
            UpdateBatches();
        }
        RenderBatches();
        previousCameraPosition = cam.transform.position;
    }
    //This method update the clouds and loops through all the batches in BatchesToUpdate list

    private void UpdateBatches() {
        foreach (var batch in batchesToUpdate) {
            foreach (var cloud in batch) {
                
                //Get noise size based on clouds pos, noise texture scale and offset amount
                float size = Mathf.PerlinNoise(cloud.x * textureScale + offsetX, cloud.y * textureScale + offsetY);

                //If the cloud has has a size above or below the visible cloud threshold it needs to be shown
                if (size > minimumNoiseSize) {

                    //Get current scale of the of the cloud 
                    float localScaleX = cloud._scale.x;

                    //Active any clouds 
                    if (!cloud.isActive) {
                        cloud.SetActive(true);
                        cloud._scale = Vector3.zero;
                    }

                    //If not maximum size, the it gets scaled up
                    if (localScaleX < maximumScale) {
                        ScaleCloud(cloud, 1);

                        //Limit the maximum size
                        if (cloud._scale.x > maximumScale) {
                            cloud._scale = new Vector3(maximumScale, maximumScale, maximumScale);
                        }
                    }
                }

                //Activate and it should not be it then scales down
                else if (size < minimumNoiseSize) {
                    float localScaleX = cloud._scale.x;
                    ScaleCloud(cloud, -1);

                    //When the cloud is very small it is set to 0 and hidden

                    if (localScaleX <= 0.1) {
                        cloud.SetActive(false);
                        cloud._scale = Vector3.zero;
                    }
                }
            }
        }
    }
    
    //This method sets the clouds to a new size

    void ScaleCloud(CloudData cloud, int direction) {
        cloud._scale += new Vector3(sizeScale * Time.deltaTime * direction, sizeScale * Time.deltaTime * direction, sizeScale * Time.deltaTime * direction);

    }

    //This method clears the batchesToUpdate list so there is only visible batches in that list
    private void UpdateBatchList() {

        //Clear the list
        batchesToUpdate.Clear();

        //Loop through all the generated batches
        foreach (var batch in batches) {

            //If a single cloud is within range the batch is added to the update list
            if (CheckForActiveBatch(batch)) {
                batchesToUpdate.Add(batch);
            }
        }
    }

    //This method loops throug all the batches to update and draws their meshes to the screen
    private void RenderBatches() {
        foreach (var batch in batchesToUpdate) {
            Graphics.DrawMeshInstanced(cloudMesh, 0, cloudMaterial, batch.Select((a) => a.matrix).ToList());
        }
    }
}
