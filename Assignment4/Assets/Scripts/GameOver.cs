﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameOver : MonoBehaviour
{
    private bool _gameEnded = false;

    // Update is called once per frame
    void Update() {
        if (_gameEnded)
            return; 

        if (PlayerHealth._lives <= 0) {
            EndGame();
        } 

    }

    void EndGame () {
        _gameEnded = true;
        //Debug.Log("GAME Over");
    }
}
