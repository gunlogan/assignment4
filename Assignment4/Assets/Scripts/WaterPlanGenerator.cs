﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WaterPlanGenerator : MonoBehaviour {

    [SerializeField]
    private float _size = 1f;
    [SerializeField]
    private int _gridSize = 16;

    private MeshFilter _filter;


    // Start is called before the first frame update
    void Start() {
        _filter = GetComponent<MeshFilter>();
        _filter.mesh = GenerateMesh();
    }

    private Mesh GenerateMesh() {

        Mesh m = new Mesh();

        var verticies = new List<Vector3>(); //stores x,y,z
        var normals = new List<Vector3>();
        var uvs = new List<Vector2>(); // only stores x,z

        for (int x = 0; x < _gridSize + 1; x++) {
            for (int y = 0; y < _gridSize + 1; y++) {
                verticies.Add(new Vector3(-_size * 0.5f + _size * (x / ((float)_gridSize)), 0, -_size * 0.5f + _size * (y / ((float)_gridSize))));
                normals.Add(Vector3.up);
                uvs.Add(new Vector2(x / (float)_gridSize, y / (float)_gridSize));
            }
        }

        var _triangles = new List<int>();
        var _vertCount = _gridSize + 1;

        for (int i = 0; i < _vertCount * _vertCount - _vertCount; i++) {
            if ((i + 1) % _vertCount == 0) {
                continue;
            }
            _triangles.AddRange(new List<int>() {
                i + 1 + _vertCount, i + _vertCount, i,
                i, i +1, i + _vertCount + 1
            });


        }
        m.SetVertices(verticies);
        m.SetNormals(normals);
        m.SetUVs(0, uvs);
        m.SetTriangles(_triangles, 0);

        return m;
    }
}
