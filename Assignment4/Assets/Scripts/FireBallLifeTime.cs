﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FireBallLifeTime : MonoBehaviour
{
    public float _timetoBurn = 5f;

    // Start is called before the first frame update
    private void Start() {
        Destroy(gameObject, _timetoBurn);
    }
}
