﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawner : MonoBehaviour
{
    public Transform _enemyWorshipperPrefab;
    public Transform _spawnPoint;

    public float _timeBetweenWaves = 10f;
    private float _countdown = 10f;

    private int _waveIndex = 1;

    private void Update () {
        if (_countdown <= 0f) {
            StartCoroutine(SpawnWave());
            _countdown = _timeBetweenWaves;
        }

        _countdown -= Time.deltaTime;
    }

    
    IEnumerator SpawnWave () {
        _waveIndex++;

        for (int i = 0; i < _waveIndex; i++) {
            SpawnEnemyWorshipper();
            yield return new WaitForSeconds(1f);
        }

    }

    void SpawnEnemyWorshipper () {
        Instantiate(_enemyWorshipperPrefab, _spawnPoint.position, _spawnPoint.rotation);
    }

}
