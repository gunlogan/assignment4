﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EnemyHealth : MonoBehaviour
{
    public float _startHealth;
    private float _health;
      

    [Header("Unity Utilities")]
    public Image _healthBar;


    void Start() {
        _health = _startHealth;        
    }
    

    public void TakeDamage (int amount) {
        _health -= amount;

        _healthBar.fillAmount = _health / _startHealth;

        if (_health <= 0)
        {
            Die();
        }
    }

    void Die() {
        Destroy(gameObject);
    }   
}
