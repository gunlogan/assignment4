﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FireBallTrigger : MonoBehaviour
{
    //public AudioClip _clip;
    //public AudioSource _audioSource;
    public OVRInput.Controller _controller;
    public GameObject _projectile;
    public float _projectileSpeed;


    /*
    private void Start()
    {
        _audioSource = GetComponent<AudioSource>();
        _audioSource.clip = _clip;
    }
    */

    protected void Update() { 
        if (OVRInput.Get(OVRInput.Axis1D.PrimaryIndexTrigger, _controller) > 0.1f) {
            //_audioSource.Play();
            Fireball();
            //Debug.Log("Pulling trigger on controller " + _controller);
        }
    }


    private void Fireball() {
        GameObject fireBall = Instantiate(_projectile, transform.position, transform.rotation) as GameObject;
        Rigidbody rb = fireBall.GetComponent<Rigidbody>();
        rb.velocity = transform.forward * _projectileSpeed;
        //Debug.Log("Pulling trigger on controller " + _controller);
    }
}
  