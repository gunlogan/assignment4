﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Projectile : MonoBehaviour
{
    public GameObject _impactEffect;

    public int _damage = 5;
    

    private void OnTriggerEnter(Collider other) {

        if (other.gameObject.tag == "Enemy") {
               GameObject effectInstance = Instantiate(_impactEffect, transform.position, transform.rotation);
                Destroy(effectInstance, 2f);

                Destroy(gameObject);
                Damage(other.transform);
                return;
        }
    }

    private void Update() {
        Destroy(gameObject, 5f);
    }

    void Damage (Transform enemy) {
        EnemyHealth e = enemy.GetComponent<EnemyHealth>();

        if(e != null) {
        e.TakeDamage(_damage);
        }
    }
}
